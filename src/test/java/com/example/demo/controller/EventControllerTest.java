package com.example.demo.controller;

import com.example.demo.controller.modelRequest.EventCreationRequest;
import com.example.demo.model.Event;
import com.example.demo.model.Ticket;
import com.example.demo.model.Venue;
import com.example.demo.service.EventService;
import com.example.demo.service.ReservationService;
import com.example.demo.service.TicketService;
import com.example.demo.service.VenueService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = EventController.class)
class EventControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EventService eventService;

    @MockBean
    TicketService ticketService;

    @MockBean
    VenueService venueService;

    @MockBean
    ReservationService reservationService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${api.version}")
    private String apiVersion;

    private EventCreationRequest eventCreationRequest;
    private Event event;
    private LocalDate date;
    private Venue venue;

    @BeforeEach
    void setUp()
    {
        this.date = LocalDate.of(Integer.parseInt("2021"), Integer.parseInt("12"), Integer.parseInt("31"));
        this.event = new Event(date, null, "Cala Vento en Niceto");
        this.eventCreationRequest = new EventCreationRequest(1, 100, 100, event);
        this.venue = new Venue("Niceto");
        Mockito.when(venueService.getVenue(eventCreationRequest.getVenueId())).thenReturn(Optional.of(venue));
    }

    @Test
    void whenValidEvent_itIsSavedAndTicketsAreCreated() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/event")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(eventCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        verify(eventService, times(1)).saveEvent(eventCaptor.capture());

        ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
        verify(ticketService, times(eventCreationRequest.getCapacity())).saveTicket(ticketArgumentCaptor.capture());

        assertThat(eventCaptor.getValue().getDate()).isEqualTo(event.getDate());
        assertThat(eventCaptor.getValue().getDescription()).isEqualTo(event.getDescription());
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    void whenNullEvent_returns400() throws Exception
    {
        this.eventCreationRequest.setEvent(null);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/event")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(eventCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    void whenNullVenueId_returns400() throws Exception
    {
        this.eventCreationRequest.setCapacity(null);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/event")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(eventCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    void whenNullCapacity_returns400() throws Exception
    {
        this.eventCreationRequest.setCapacity(null);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/event")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(eventCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Event> clientCaptor = ArgumentCaptor.forClass(Event.class);
        verify(eventService, times(0)).saveEvent(clientCaptor.capture());

        ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
        verify(ticketService, times(0)).saveTicket(ticketArgumentCaptor.capture());

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    void whenNullPrice_returns400() throws Exception
    {
        this.eventCreationRequest.setPrice(null);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/event")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(eventCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Event> clientCaptor = ArgumentCaptor.forClass(Event.class);
        verify(eventService, times(0)).saveEvent(clientCaptor.capture());

        ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
        verify(ticketService, times(0)).saveTicket(ticketArgumentCaptor.capture());

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }
}
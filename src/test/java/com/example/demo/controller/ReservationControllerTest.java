package com.example.demo.controller;

import com.example.demo.controller.modelRequest.ReservationCreationRequest;
import com.example.demo.model.*;
import com.example.demo.service.ClientService;
import com.example.demo.service.EventService;
import com.example.demo.service.ReservationService;
import com.example.demo.service.TicketService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@WebMvcTest(controllers = ReservationController.class)
class ReservationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ReservationService reservationService;

    @MockBean
    TicketService ticketService;

    @MockBean
    ClientService clientService;

    @MockBean
    EventService eventService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${api.version}")
    private String apiVersion;

    private ReservationCreationRequest reservationCreationRequest;

    private Event event;
    private Venue venue;
    private LocalDate date;
    private Client client;
    private ArrayList<Ticket> tickets;

    @BeforeEach
    void setUp()
    {
        this.tickets = new ArrayList<>();
        this.reservationCreationRequest = new ReservationCreationRequest("user", "mail@mail.com",
                                                                                                1, 10);
        this.date = LocalDate.of(Integer.parseInt("2021"), Integer.parseInt("12"), Integer.parseInt("31"));
        this.venue = new Venue("Niceto");
        this.event = new Event(date, venue, "David Bowie en Niceto");

        for (int i = 0; i < (reservationCreationRequest.getNumberOfTickets() + 10); i++ )
        {
            Ticket ticket = new Ticket(100, null, event);
            this.tickets.add(ticket);
        }

        this.event.setTickets(tickets);

        this.client = new Client(reservationCreationRequest.getName(), reservationCreationRequest.getEmail());

        Mockito.when(eventService.getEvent(reservationCreationRequest.getIdEvent())).thenReturn(Optional.of(event));
        Mockito.when(ticketService.getAvailableTickets(reservationCreationRequest.getIdEvent())).thenReturn(tickets);
        Mockito.when(ticketService.getAvailableTickets(reservationCreationRequest.getIdEvent(),
                                                        reservationCreationRequest.getNumberOfTickets())).thenReturn(tickets);
        Mockito.when(reservationService.calculateFinalPrice(tickets)).thenReturn(1000d);
        Mockito.when(clientService.findByNameAndEmailEquals(reservationCreationRequest.getName(),
                reservationCreationRequest.getEmail())).thenReturn(Optional.ofNullable(client));
    }

    @Test
    void whenValidReservation_itIsSavedAndTicketsAreCreated() throws Exception
    {
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/reservation")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationCreationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Reservation> reservationArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);

        verify(reservationService, times(1)).saveReservation(reservationArgumentCaptor.capture());

        assertThat(reservationArgumentCaptor.getValue().getClient()).isEqualTo(client);
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }


}

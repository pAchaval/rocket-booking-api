package com.example.demo.controller;

import com.example.demo.model.Client;
import com.example.demo.service.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ClientController.class)
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ClientService clientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${api.version}")
    private String apiVersion;

    @Test
    void whenNullName_returns400() throws Exception
    {
        Client clientNullName = new Client(null, "email@provider.com");
        mockMvc.perform(post("/api/"+ apiVersion +"/client")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(clientNullName)))
                .andExpect(status().isBadRequest())
                .andReturn();

    }

    @Test
    void whenEmptyName_returns400() throws Exception
    {
        Client clientNullName = new Client("", "email@provider.com");
        mockMvc.perform(post("/api/"+ apiVersion +"/client")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(clientNullName)))
                .andExpect(status().isBadRequest())
                .andReturn();

    }

    @Test
    void whenNullEmail_returns400() throws Exception
    {
        Client clientNullEmail = new Client("User", null);
        mockMvc.perform(post("/api/"+ apiVersion +"/client")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(clientNullEmail)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }


    @Test
    void whenEmptyEmail_returns400() throws Exception
    {
        Client clientEmptyEmail = new Client("User", "");
        mockMvc.perform(post("/api/"+ apiVersion +"/client")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(clientEmptyEmail)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void whenValidClient_returns201() throws Exception
    {
        Client client = new Client("User", "user@provider.com");

        mockMvc.perform(post("/api/"+ apiVersion +"/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(client))
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated());
    }

    @Test
    void whenValidInputClientIsSaved() throws Exception
    {
        Client client = new Client("User", "user@provider.com");

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/client")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(client))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");


        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Client> clientCaptor = ArgumentCaptor.forClass(Client.class);
        verify(clientService, times(1)).saveClient(clientCaptor.capture());

        assertThat(clientCaptor.getValue().getName()).isEqualTo(client.getName());
        assertThat(clientCaptor.getValue().getEmail()).isEqualTo(client.getEmail());
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

}
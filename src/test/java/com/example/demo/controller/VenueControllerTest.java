package com.example.demo.controller;

import com.example.demo.model.Venue;
import com.example.demo.service.VenueService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = VenueController.class)
class VenueControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    VenueService venueService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${api.version}")
    private String apiVersion;

    @Test
    void whenNullDescription_returns400() throws Exception
    {
        Venue venueNull = new Venue(null);
        mockMvc.perform(post("/api/"+ apiVersion +"/venue")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(venueNull)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void whenEmptyDescription_returns400() throws Exception
    {
        Venue venueEmpty = new Venue("");
        mockMvc.perform(post("/api/"+ apiVersion +"/venue")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(venueEmpty)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void whenValidVenues_returns201() throws Exception
    {
        Venue venue = new Venue("Niceto");

        mockMvc.perform(post("/api/"+ apiVersion +"/venue")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(venue))
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated());
    }

    @Test
    void whenValidInputVenueIsSaved() throws Exception
    {
        Venue venue = new Venue("Niceto");

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/"+ apiVersion +"/venue")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(venue))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        ArgumentCaptor<Venue> venueCaptor = ArgumentCaptor.forClass(Venue.class);

        verify(venueService, times(1)).saveVenue(venueCaptor.capture());
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertThat(venueCaptor.getValue().getDescription()).isEqualTo(venue.getDescription());
    }
}
